       IDENTIFICATION DIVISION. 
       PROGRAM-ID. AVG-GRADE.
       AUTHOR. THANAWAT.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT MY-GRADE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT GRADE-FILE ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD MY-GRADE-FILE.
       01  MY-GRADE-DETAIL.
           88 END-OF-MY-GRADE-FILE VALUE HIGH-VALUE.
           05 SUBJECT-ID.
              10 FIRST-DIGIT-ID PIC X(1). 
              10 SECOND-DIGIT-ID PIC X(1). 
              10 OTHER-DIGIT-ID PIC X(4). 
           05 SUBJECT-NAME PIC X(50).
           05 SUBJECT-UNIT PIC 9(1).
           05 SUBJECT-GRADE PIC X(2).
       FD GRADE-FILE.
       01  GRADE-DETAIL.
           05 DETAIL-NAME PIC X(15).
           05 AVG         PIC 9.999.
      
       WORKING-STORAGE SECTION. 
       01  SUM-UNIT    PIC 9(3) VALUE ZEROS.
       01  SUM-UNIT-SCI    PIC 9(3) VALUE ZEROS.
       01  SUM-UNIT-CS    PIC 9(3) VALUE ZEROS.
       01  SUM-SCORE-ALL PIC 9(3)V9(2) VALUE ZEROS.
       01  SUM-SCORE-SCI PIC 9(3)V9(2) VALUE ZEROS.
       01  SUM-SCORE-CS PIC 9(3)V9(2) VALUE ZEROS.
       01  GRADE-CON PIC 9(3)V9(2) VALUE ZEROS.
       01  GRADE       PIC 9(3)V9(3).
       01  GRADE-SCI       PIC 9(3)V9(3).
       01  GRADE-CS       PIC 9(3)V9(3).
       01  TITLE-NAME PIC X(15).

       PROCEDURE DIVISION.
       000-BEGIN.
           OPEN INPUT MY-GRADE-FILE 
           OPEN OUTPUT GRADE-FILE 
              PERFORM UNTIL END-OF-MY-GRADE-FILE
                 READ MY-GRADE-FILE
                    AT END SET END-OF-MY-GRADE-FILE TO TRUE
                 END-READ
                 IF NOT END-OF-MY-GRADE-FILE THEN
                    MOVE "AVG-GRADE" TO TITLE-NAME
                    PERFORM 001-PROCESS THRU 004-EXIT
                 END-IF
              END-PERFORM
             PERFORM 008-WRITE-UNIT THRU 008-EXIT
           CLOSE MY-GRADE-FILE 
           CLOSE GRADE-FILE
           GOBACK
       .
       001-PROCESS.
      *    DISPLAY "-> "SUM-UNIT " " SUM-SCORE-ALL " " GRADE
           COMPUTE SUM-UNIT = SUM-UNIT + SUBJECT-UNIT
           
           EVALUATE TRUE 
              WHEN SUBJECT-GRADE = "A " MOVE 4 TO GRADE-CON
              WHEN SUBJECT-GRADE = "B+" MOVE 3.5 TO GRADE-CON
              WHEN SUBJECT-GRADE = "B " MOVE 3 TO GRADE-CON
              WHEN SUBJECT-GRADE = "C+" MOVE 2.5 TO GRADE-CON
              WHEN SUBJECT-GRADE = "C " MOVE 2 TO GRADE-CON
              WHEN SUBJECT-GRADE = "D+" MOVE 1.5 TO GRADE-CON
              WHEN SUBJECT-GRADE = "D " MOVE 1 TO GRADE-CON
              WHEN SUBJECT-GRADE = "F " MOVE 0.5 TO GRADE-CON
           END-EVALUATE
           
           COMPUTE SUM-SCORE-ALL = SUM-SCORE-ALL + 
           (SUBJECT-UNIT * GRADE-CON) 

           COMPUTE GRADE = SUM-SCORE-ALL / SUM-UNIT 

      *    DISPLAY FIRST-DIGIT-ID SECOND-DIGIT-ID OTHER-DIGIT-ID  
      *       " " SUM-SCORE-ALL
      *       " " GRADE
           
       .
       001-EXIT.
           EXIT
       .

       003-PROCESS.
           IF FIRST-DIGIT-ID = 3 THEN 
      *       DISPLAY "-> "SUM-UNIT-SCI " " SUM-SCORE-SCI " " GRADE-SCI
              COMPUTE SUM-UNIT-SCI = SUM-UNIT-SCI + SUBJECT-UNIT
              
              EVALUATE TRUE 
                 WHEN SUBJECT-GRADE = "A " MOVE 4 TO GRADE-CON
                 WHEN SUBJECT-GRADE = "B+" MOVE 3.5 TO GRADE-CON
                 WHEN SUBJECT-GRADE = "B " MOVE 3 TO GRADE-CON
                 WHEN SUBJECT-GRADE = "C+" MOVE 2.5 TO GRADE-CON
                 WHEN SUBJECT-GRADE = "C " MOVE 2 TO GRADE-CON
                 WHEN SUBJECT-GRADE = "D+" MOVE 1.5 TO GRADE-CON
                 WHEN SUBJECT-GRADE = "D " MOVE 1 TO GRADE-CON
                 WHEN SUBJECT-GRADE = "F " MOVE 0.5 TO GRADE-CON
              END-EVALUATE
              
              COMPUTE SUM-SCORE-SCI = SUM-SCORE-SCI + 
              (SUBJECT-UNIT * GRADE-CON) 
   
              COMPUTE GRADE-SCI = SUM-SCORE-SCI / SUM-UNIT-SCI 
   
      *       DISPLAY FIRST-DIGIT-ID SECOND-DIGIT-ID OTHER-DIGIT-ID  
      *       " " SUM-SCORE-SCI
      *       " " GRADE-SCI
           END-IF 
       .
       003-EXIT.
           EXIT
       .
       004-PROCESS.
           IF FIRST-DIGIT-ID = 3 THEN 
              IF SECOND-DIGIT-ID = 1 THEN
      *       DISPLAY "-> "SUM-UNIT-CS " " SUM-SCORE-CS " " GRADE-CS
              COMPUTE SUM-UNIT-CS = SUM-UNIT-CS + SUBJECT-UNIT
              
              EVALUATE TRUE 
                 WHEN SUBJECT-GRADE = "A " MOVE 4 TO GRADE-CON
                 WHEN SUBJECT-GRADE = "B+" MOVE 3.5 TO GRADE-CON
                 WHEN SUBJECT-GRADE = "B " MOVE 3 TO GRADE-CON
                 WHEN SUBJECT-GRADE = "C+" MOVE 2.5 TO GRADE-CON
                 WHEN SUBJECT-GRADE = "C " MOVE 2 TO GRADE-CON
                 WHEN SUBJECT-GRADE = "D+" MOVE 1.5 TO GRADE-CON
                 WHEN SUBJECT-GRADE = "D " MOVE 1 TO GRADE-CON
                 WHEN SUBJECT-GRADE = "F " MOVE 0.5 TO GRADE-CON
              END-EVALUATE
              
              COMPUTE SUM-SCORE-CS = SUM-SCORE-CS + 
              (SUBJECT-UNIT * GRADE-CON) 
   
              COMPUTE GRADE-CS = SUM-SCORE-CS / SUM-UNIT-CS 
   
      *       DISPLAY FIRST-DIGIT-ID SECOND-DIGIT-ID OTHER-DIGIT-ID  
      *       " " SUM-SCORE-CS
      *       " " GRADE-CS
              END-IF
           END-IF 
       .
       004-EXIT.
           EXIT
       .
       008-WRITE-UNIT.
           MOVE "AVG-GRADE" TO DETAIL-NAME IN GRADE-DETAIL
           MOVE GRADE TO AVG
           WRITE GRADE-DETAIL   
           MOVE "AVG-SCI-GRADE" TO DETAIL-NAME IN GRADE-DETAIL
           MOVE GRADE-SCI TO AVG
           WRITE GRADE-DETAIL 
           MOVE "AVG-CS-GRADE" TO DETAIL-NAME IN GRADE-DETAIL
           MOVE GRADE-CS TO AVG
           WRITE GRADE-DETAIL     
       .
       008-EXIT.
           DISPLAY "End Calculate process."
           DISPLAY "End Write file process."
           DISPLAY "End Program."
           EXIT
       .

